import styled from 'styled-components';

const ButtonsContainer = styled.div`
  display: flex;
  justify-content: space-between;
  margin: 32px 0 0;
`;

export default ButtonsContainer;
