import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import colors from 'utils/css/colors';
import { useTranslation } from 'hooks';

import locales from './i18n';

const InputWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

const InputLabel = styled.span`
  color: ${colors.blackRed};
  font-size: 18px;
  font-weight: 500;
  margin-bottom: 12px;
`;

const InputComponent = styled.input.attrs(props => ({
  type: props.type || 'text',
}))`
  font-size: 16px;
  font-weight: 500;
  line-height: 56px;
  padding: 0 24px;
  margin: 0 0 10px;
  border: 1px solid ${props => (props.hasError ? 'red' : '#F5F0F0')};
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.04), 0px 4px 8px rgba(0, 0, 0, 0.08);
  border-radius: 8px;
  transition: all 250ms ease-out;
  &:focus {
    outline: none;
  }
`;

const InputRequeriments = styled.span`
  font-size: 14px;
  color: #A8A8A8;
`;

const Input = React.forwardRef(({
  value,
  label,
  maxChars,
  error,
  onChange,
  ...rest
}, ref) => {
  const { t } = useTranslation(locales);

  const hasError = (error || (maxChars && value.length > maxChars)) && value !== '';

  return (
    <InputWrapper>
      {label && <InputLabel>{label}</InputLabel>}
      <InputComponent
        ref={ref}
        value={value}
        onChange={onChange}
        hasError={hasError}
        {...rest}
      />
      {maxChars && <InputRequeriments>{t('Max {{maxChars}} chars', { maxChars })}</InputRequeriments>}
      {error !== '' && <div>{error}</div>}
    </InputWrapper>
  );
});

Input.propTypes = {
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]).isRequired,
  label: PropTypes.string,
  maxChars: PropTypes.number,
  error: PropTypes.string,
  onChange: PropTypes.func,
};

Input.defaultProps = {
  label: null,
  error: null,
  maxChars: null,
  onChange: null,
};

export default Input;
