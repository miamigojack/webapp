import styled from 'styled-components';

const TitleWrapper = styled.div`
  text-align: center;
  margin: 0 0 8px;
`;

export default TitleWrapper;
