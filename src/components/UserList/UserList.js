import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { FixedSizeList as List } from 'react-window';
import { useDispatch } from 'react-redux';

import { useElementHeight } from 'hooks';

import Spinner from 'components/Spinner';
import UserLink from 'components/UserLink';
import UserDisplayName from 'components/UserDisplayName';
import UserAvatar from 'components/UserAvatar';

const ListWrapper = styled.div`
  height: 100%;
  overflow: hidden;
  position: relative;
`;
ListWrapper.displayName = 'ListWrapper';

const UserWrapper = styled.div`
  display: flex;
  padding: 8px 0;
  position: relative;

  .avatar {
    margin-right: 8px;
  }

  &:hover {
    .displayname {
      color: ${props => props.theme.colors.main};
    }
  }
`;
UserWrapper.displayName = 'UserWrapper';

const areRowsEqual = (prevProps, nextProps) => {
  const prevUserId = prevProps.data.userIds[prevProps.index];
  const nextUserId = nextProps.data.userIds[nextProps.index];

  return prevUserId === nextUserId;
};

const UserRow = React.memo(({ data: { userIds, onUserClick }, index, style }) => {
  const uId = userIds[index];
  return (
    <UserLink key={`user-list-${uId}`} style={style} userId={uId} onClick={onUserClick}>
      <UserWrapper>
        <UserAvatar userId={uId} />
        <UserDisplayName userId={uId} />
      </UserWrapper>
    </UserLink>
  );
}, areRowsEqual);

UserRow.propTypes = {
  data: PropTypes.shape({
    userIds: PropTypes.arrayOf(PropTypes.number).isRequired,
    onUserClick: PropTypes.func.isRequired,
  }).isRequired,
  index: PropTypes.number.isRequired,
  style: PropTypes.shape({}).isRequired,
};

const UserList = ({ fetchAction, onUserClick }) => {
  const dispatch = useDispatch();

  const [listHeight, selectableListEl] = useElementHeight();
  const [userIds, setUserIds] = useState(null);

  useEffect(() => {
    const fetch = async () => {
      const ids = await dispatch(fetchAction());
      setUserIds(ids);
    };

    fetch();
  }, [dispatch, fetchAction]);

  return (
    <ListWrapper ref={selectableListEl}>
      {userIds === null
        ? <Spinner color="#999" />
        : (
          <List
            height={listHeight}
            itemCount={userIds.length}
            itemSize={66}
            itemData={{ userIds, onUserClick }}
            width="100%"
          >
            {UserRow}
          </List>
        )
      }
    </ListWrapper>
  );
};

UserList.propTypes = {
  fetchAction: PropTypes.func.isRequired,
  onUserClick: PropTypes.func.isRequired,
};

UserList.defaultProps = {
};

export default UserList;
