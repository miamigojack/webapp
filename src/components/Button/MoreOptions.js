import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import ChevronDown from 'components/Icons/ChevronDown';

const CaretContainer = styled.div`
  border-left: 1px solid rgba(0, 0, 0, 0.1);
  padding: 0 8px 0 0;
  position: absolute;
  top: 0;
  right: 0;
  height: 34px;
  display: flex;
  justify-content: center;
  flex-direction: column;

  svg.chevron {
    width: 24px;
    height: 24px;
    margin: -1px 0 0 4px;
  }
`;

const MoreOptions = ({ onClick, disabled, negative }) => {
  let color = '#efefef';
  if (negative) color = 'black';
  if (disabled) color = '#aaa';

  return (
    <CaretContainer onClick={onClick} disabled={disabled}>
      <ChevronDown color={color} className="chevron" />
    </CaretContainer>
  );
};

MoreOptions.propTypes = {
  onClick: PropTypes.func.isRequired,
  disabled: PropTypes.bool.isRequired,
  negative: PropTypes.bool.isRequired,
};

MoreOptions.defaultProps = {
};

export default MoreOptions;
