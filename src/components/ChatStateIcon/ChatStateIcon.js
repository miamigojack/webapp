import React from 'react';
import PropTypes from 'prop-types';

import { ProgressClock, Check, DoubleCheck } from 'components/Icons';

const ChatStateIcon = React.memo(({ state }) => {
  switch (state) {
    case 'SENT':
      return <Check color="#666" />;
    case 'RECEIVED':
      return <DoubleCheck color="#666" />;
    case 'READ':
      return <DoubleCheck color="#4FC3F7" />;
    case 'SENDING':
    default:
      return <ProgressClock color="#666" />;
  }
});

ChatStateIcon.propTypes = {
  state: PropTypes.oneOf(['SENT', 'RECEIVED', 'READ', 'SENDING']).isRequired,
};

ChatStateIcon.defaultProps = {
};

export default ChatStateIcon;
