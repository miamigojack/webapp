import React from 'react';

import { IconType, IconDefaults } from './type';

const normalPath = 'M3 3V21H21V3H3M18 18H6V17H18V18M18 16H6V15H18V16M18 12H6V6H18V12Z';
const outlinePath = 'M19 5V19H5V5H19M21 3H3V21H21V3M17 17H7V16H17V17M17 15H7V14H17V15M17 12H7V7H17V12Z';

const Post = ({ color, className, outline }) => (
  <svg viewBox="0 0 24 24" className={className}>
    <path
      d={outline ? outlinePath : normalPath}
      fill={color}
    />
  </svg>
);

Post.propTypes = IconType;
Post.defaultProps = IconDefaults;

export default Post;
