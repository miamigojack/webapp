import React from 'react';

// eslint-disable-next-line jsx-a11y/alt-text
const Image = props => <img {...props} />;

Image.propTypes = {
};

Image.defaultProps = {
};

export default Image;
