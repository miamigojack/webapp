import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import styled from 'styled-components';

import * as appSelectors from 'state/app/selectors';
import * as userSelectors from 'state/users/selectors';

import Online from './Online';
import Badges from './Badges';
import defaultAvatar from './default.png';

const AvatarWrapper = styled.div.attrs({
  className: 'avatar',
})`
  position: relative;
  width: ${props => props.size};
  height: ${props => props.size};

  ${props => props.onClick && `
    cursor: pointer;
  `}
`;

const AvatarImg = styled.img.attrs(props => ({
  width: props.size,
  height: props.size,
}))`
  border-radius: 100%;
  width: ${props => props.size};
  height: ${props => props.size};
  box-shadow: 0px 2px 7px rgba(0, 0, 0, 0.12);
  flex-shrink: 0;
  vertical-align: middle;
`;

const userEqual = (prevUser, nextUser) => (
  prevUser.id === nextUser.id
  && prevUser.avatar === nextUser.avatar
  && prevUser.loading === nextUser.loading
);

const UserAvatar = ({
  userId,
  size,
  showOnline,
  onlineBorderColor,
  ...props
}) => {
  const user = useSelector(userSelectors.getById(userId), userEqual);
  const isSafeForWork = useSelector(appSelectors.selectIsSafeForWork);

  const src = !user || user.loading || isSafeForWork || !user.avatar || !user.avatar['150w'] ? defaultAvatar : user.avatar['150w'].jpeg;

  return (
    <AvatarWrapper size={size} {...props}>
      <AvatarImg src={src} size={size} />
      {showOnline && <Online size={size} userId={userId} borderColor={onlineBorderColor} />}
      {showOnline && <Badges size={size} userId={userId} borderColor={onlineBorderColor} />}
    </AvatarWrapper>
  );
};

UserAvatar.propTypes = {
  userId: PropTypes.number.isRequired,
  size: PropTypes.string,
  showOnline: PropTypes.bool,
  onlineBorderColor: PropTypes.string,
};

UserAvatar.defaultProps = {
  size: '50px',
  showOnline: true,
  onlineBorderColor: 'white',
};

const equality = (prevProps, nextProps) => (
  prevProps.userId === nextProps.userId
  && prevProps.size === nextProps.size
  && prevProps.showOnline === nextProps.showOnline
  && prevProps.onlineBorderColor === nextProps.onlineBorderColor
);
export default React.memo(UserAvatar, equality);
