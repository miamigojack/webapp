import styled from 'styled-components';

const InfoItem = styled.span`
  margin-right: 15px;

  svg {
    width: 24px;
    margin-right: 8px;
  }
`;

export default InfoItem;
