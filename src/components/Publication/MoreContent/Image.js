import React from 'react';
import PropTypes from 'prop-types';

const Image = ({
  optimized, src, path, alt, width, height, ...rest
}) => {
  if (!optimized || !optimized.length) {
    return <img src={src} width={width} height={height} alt={alt} {...rest} />;
  }

  const data = { src, width, height };

  return (
    <picture>
      {optimized.reverse().map((o) => {
        const parts = o.filename.split('-');
        const w = parseInt(parts[parts.length - 1].split('.')[0].replace('w', ''), 10);
        if (w === 668 && o.type === 'jpeg') {
          data.src = `${path}/${o.filename}`;
          data.width = o.metadata.width;
          data.height = o.metadata.height;
        }

        return (
          <source
            key={o.filename}
            type={`image/${o.type}`}
            srcSet={`${path}/${o.filename} ${o.metadata.width}w`}
            media={w < 668 ? `(max-width: ${o.metadata.width}px)` : undefined}
          />
        );
      })}

      <img
        src={data.src}
        width={data.width}
        height={data.height}
        alt={alt}
        {...rest}
      />
    </picture>
  );
};

Image.propTypes = {
  src: PropTypes.string.isRequired,
  alt: PropTypes.string.isRequired,
  optimized: PropTypes.arrayOf(PropTypes.shape({
    type: PropTypes.oneOf(['webp', 'jpeg']).isRequired,
    filename: PropTypes.string.isRequired,
    metadata: PropTypes.shape({
      width: PropTypes.number.isRequired,
      height: PropTypes.number.isRequired,
    }).isRequired,
  })),
  path: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

Image.defaultProps = {
  optimized: null,
  path: '',
  width: null,
  height: null,
};

export default Image;
