import styled from 'styled-components';

const FakeComposer = styled.div`
  color: #999;
  font-size: 12px;
`;
FakeComposer.displayName = 'FakeComposer';

export default FakeComposer;
