import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const WarningWrapper = styled.div`
  color: #856404;
  background-color: #fff3cd;
  border: 1px solid #ffeeba;
  border-radius: .25rem;
  margin: 16px 0px;
  padding: 8px;

  .actions {
    margin-top: 16px;

    > a:not(:last-child), button:not(:last-child) {
      margin-right: 16px;
    }
  }
`;
WarningWrapper.displayName = 'WarningWrapper';

const Warning = ({ children }) => (
  <WarningWrapper>
    {children}
  </WarningWrapper>
);

Warning.propTypes = {
  children: PropTypes.node.isRequired,
};

Warning.defaultProps = {
};

export default Warning;
