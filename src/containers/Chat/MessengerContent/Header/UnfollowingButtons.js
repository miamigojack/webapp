import styled from 'styled-components';

const UnfollowingButtons = styled.div`
  button {
    margin: 0 8px;
  }

  @media(max-width: 768px) {
    position: fixed;
    top: 64px;
    width: 100%;
    left: 0;
    background-color: rgba(0, 0, 0, .1);
    padding: 12px 0;
    box-sizing: border-box;
    display: flex;
    justify-content: space-around;

    button {
      margin: 0;
    }
  }
`;
UnfollowingButtons.displayName = 'UnfollowingButtons';

export default UnfollowingButtons;
