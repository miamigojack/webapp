import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useDispatch } from 'react-redux';

import * as appActions from 'state/app/actions';
import * as channelActions from 'state/channels/actions';
import { useTranslation, useSearchUsers } from 'hooks';

import Modal from 'components/Modal';
import { UserSearchInput, UserSearchResults } from 'components/UserSearch';
import { SelectableList, SelectableListItem } from 'components/SelectableList';
import UserAvatar from 'components/UserAvatar';
import Button from 'components/Button';
import { Close } from 'components/Icons';

import locales from '../../i18n';

const SearchContainer = styled.div`
  position: relative;
  width: 100%;

  .search-container {
    background-color: white;
    box-shadow: 0px 0px 5px #9999;

    input {
      color: black;

      &::placeholder {
        color: #666;
      }
    }
  }
`;

const ResultsContainer = styled.div`
  position: relative;
  width: 100%;
`;

const AddInviteModal = ({ channelId, close }) => {
  const dispatch = useDispatch();
  const { t } = useTranslation(locales);

  const [searchProps, results, reset] = useSearchUsers();
  const [selectedUser, setSelectedUser] = useState(null);
  const [inviting, setInviting] = useState(false);

  const addInvite = user => setSelectedUser(user);
  const clearSelectedUser = () => {
    reset();
    setSelectedUser(null);
  };

  const invite = async () => {
    try {
      setInviting(true);
      await dispatch(channelActions.invite(channelId, selectedUser.id));
      dispatch(appActions.addToast(t('Invitation sent')));
      close();
    } catch (error) {
      dispatch(appActions.addError(error));
      close();
    }
  };

  const modalActions = [];
  if (selectedUser) modalActions.push(<Button key="add-invite-confirm" onClick={invite} loading={inviting}>{t('global:Confirm')}</Button>);

  return (
    <Modal
      title={t('Send invitation')}
      onCancel={close}
      actions={modalActions}
    >
      {!selectedUser
        ? (
          <>
            <SearchContainer>
              <UserSearchInput {...searchProps} onBlurTriggered={reset} />
            </SearchContainer>
            <ResultsContainer>
              {results.length > 0 && <UserSearchResults results={results} onSelect={addInvite} />}
            </ResultsContainer>
          </>
        ) : (
          <SelectableList>
            <SelectableListItem
              avatar={(<UserAvatar userId={selectedUser.id} />)}
              title={selectedUser.displayname}
              actions={[<Button key="remove-invite-confirm" onClick={clearSelectedUser} color="white" light><Close /></Button>]}
            />
          </SelectableList>
        )
      }
    </Modal>
  );
};

AddInviteModal.propTypes = {
  channelId: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired,
};

export default AddInviteModal;
