import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

import * as channelSelectors from 'state/channels/selectors';

import Time from './Time';
import Reactions from './Reactions';
import InfoComponent from '../Info';

const Info = ({ messageId }) => {
  const hasReactions = useSelector(state => channelSelectors.messageHasReactions(state, messageId));

  return (
    <InfoComponent hasReactions={hasReactions}>
      <Reactions messageId={messageId} key={`reactions-${messageId}`} />
      <div><Time messageId={messageId} /></div>
    </InfoComponent>
  );
};

Info.propTypes = {
  messageId: PropTypes.string.isRequired,
};

Info.defaultProps = {
};

export default Info;
