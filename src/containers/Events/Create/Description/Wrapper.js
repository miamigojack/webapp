import styled from 'styled-components';

const DescriptionWrapper = styled.div`
  h3 {
    color: #666;
    margin-bottom: 8px;
    font-weight: 300;
  }

  .communities {
    > div {
      z-index: 10;
    }
  }

  .composer {
    padding: 16px;
    margin: 0 0 10px;
    border: 1px solid #F5F0F0;
    box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.04), 0px 4px 8px rgba(0, 0, 0, 0.08);
    border-radius: 8px;
  }

  .actions {
    display: flex;
    align-items: center;

    > div {
      margin-right: 16px;

      svg {
        width: 32px;
        height: 32px;
      }
    }

  }
`;
DescriptionWrapper.displayName = 'DescriptionWrapper';

export default DescriptionWrapper;
