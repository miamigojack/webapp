import React, { useCallback, useState, useRef } from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';

import { useTranslation, useOpenClose } from 'hooks';
import * as eventSelectors from 'state/events/selectors';
import * as eventActions from 'state/events/actions';
import * as appActions from 'state/app/actions';

import Button from 'components/Button';

import Instructions from './Instructions';
import locales from '../i18n';

const ButtonRSVP = ({ eventId }) => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();

  const rsvpd = useSelector(state => eventSelectors.hasRSVPd(state, eventId));

  const suggestShare = useRef(false);
  const [rsvping, setRsvping] = useState(false);
  const [instrModalOpened, openInstructionsModal, closeInstructionsModal] = useOpenClose(false);

  const rsvp = useCallback(async () => {
    try {
      setRsvping(true);
      await dispatch(eventActions.rsvp(eventId));
      suggestShare.current = true;
      openInstructionsModal();
    } catch (error) {
      dispatch(appActions.addError(error));
    }

    setRsvping(false);
  }, [dispatch, eventId, openInstructionsModal]);

  return (
    <>
      {rsvpd ? (
        <Button onClick={openInstructionsModal} color="white" fontColor="black">{t('Instructions')}</Button>
      ) : (
        <Button onClick={rsvp} loading={rsvping}>{t('RSVP')}</Button>
      )}

      {instrModalOpened && (
        <Instructions
          eventId={eventId}
          close={closeInstructionsModal}
          suggestShare={suggestShare.current}
        />
      )}
    </>
  );
};

ButtonRSVP.propTypes = {
  eventId: PropTypes.string.isRequired,
};

ButtonRSVP.defaultProps = {
};

export default ButtonRSVP;
