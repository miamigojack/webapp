import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';

import colors from 'utils/css/colors';
import { useTranslation } from 'hooks';
import * as replySelectors from 'state/replies/selectors';
import * as replyActions from 'state/replies/actions';
import * as threadSelectors from 'state/threads/selectors';
import * as threadActions from 'state/threads/actions';

import Hoverable from 'components/Hoverable';
import { Spank } from 'components/Icons';

import ActionIcon from './ActionIcon';
import Share from './Share';
import locales from './i18n';

const ActionWrapper = styled.div`
  display: flex;
  align-items: center;
  margin: 24px 16px;

  > div {
    margin-left: auto;
    display: flex;
  }
`;

const Actions = ({ entityId, type }) => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();

  const selectors = type === 'reply' ? replySelectors : threadSelectors;
  const actions = type === 'reply' ? replyActions : threadActions;

  const isSpankedByUser = useSelector(selectors.isSpankedByUser(entityId));
  const reactionsCount = useSelector(selectors.getReactionsCount(entityId));
  const isDeleted = useSelector(selectors.isDeleted(entityId));

  const [spanking, setSpanking] = useState(false);
  const [unspanking, setUnspanking] = useState(false);

  const spank = async () => {
    try {
      setSpanking(true);
      await dispatch(actions.createReaction(entityId));
      setSpanking(false);
    } catch (e) {
      setSpanking(false);
    }
  };

  const unspank = async () => {
    try {
      setUnspanking(true);
      await dispatch(actions.deleteReaction(entityId));
      setUnspanking(false);
    } catch (e) {
      setUnspanking(false);
    }
  };

  if (isDeleted) return null;

  const unspanked = (!isSpankedByUser && !spanking) || unspanking;
  const onReactionClick = unspanked ? spank : unspank;

  return (
    <ActionWrapper>
      <div>
        {type === 'thread' && (
          <Share threadId={entityId} />
        )}

        <ActionIcon onClick={onReactionClick} animate>
          <Hoverable
            normal={<Spank color={colors.red} outline={unspanked} />}
            hover={<Spank color={colors.red} outline={!unspanked} />}
          />
        </ActionIcon>
        <ActionIcon onClick={onReactionClick}>
          <span>{t('global:Spank')}</span>
          {reactionsCount > 0 && <span>{` • ${reactionsCount}`}</span>}
        </ActionIcon>
      </div>
    </ActionWrapper>
  );
};

Actions.propTypes = {
  entityId: PropTypes.string.isRequired,
  type: PropTypes.oneOf(['thread', 'reply']).isRequired,
};

Actions.defaultProps = {
};

export default Actions;
