import React from 'react';
import styled from 'styled-components';

import { useTranslation } from 'hooks';

import SignupForm from 'containers/Landing/SignupForm';
import Header from 'components/Header';
import EmptyState from 'components/EmptyState';
import Button from 'components/Button';

import { GUEST_MAX_THREADS } from '../../../constants';
import locales from './i18n';

const Wrapper = styled.div`
  padding: 32px;
  background: white;
  box-sizing: border-box;

  .emptystate {
    @media(max-width: 767px) {
      margin-top: 64px;
    }
  }

  .signup-form {
    max-width: 500px;
    margin: 0 auto;
    display: block;
    margin-top: 32px;

    div {
      width: auto;
    }

    @media(max-width: 767px) {
      padding: 0;
      min-width: auto;
    }
  }
`;
Wrapper.displayName = 'Wrapper';

const NeedsAnAccount = () => {
  const { t } = useTranslation(locales);

  return (
    <Wrapper>
      <Header mobileOnly />

      <EmptyState
        title={t('You reached your view limit for today')}
        subtitle={t('Unregistered visits are only allowed to a maximum of {{max}} thread views in a day. Do you like what you are reading? Create an account! Is fast, easy and free.', { max: GUEST_MAX_THREADS })}
      >
        <SignupForm />
        <Button className="empty" to="/login">{t('Already have an account')}</Button>
      </EmptyState>
    </Wrapper>
  );
};

NeedsAnAccount.propTypes = {
};

NeedsAnAccount.defaultProps = {
};

export default NeedsAnAccount;
