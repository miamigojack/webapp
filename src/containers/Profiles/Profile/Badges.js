import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useSelector, shallowEqual } from 'react-redux';

import getBadgeColor from 'utils/getBadgeColor';
import { useTranslation } from 'hooks';
import * as userSelectors from 'state/users/selectors';

import Decagram from 'components/Icons/Decagram';

import locales from '../i18n';
import { BADGES } from '../../../constants';

const getBadgeValue = badge => BADGES[badge]?.value || 0;

const Wrapper = styled.div`

`;
Wrapper.displayName = 'Wrapper';

const Badge = styled.div`
  margin: 0 8px;

  svg {
    height: 64px;
    width: 64px;

    path {
      fill: ${props => getBadgeColor({ ...props, badgesValue: getBadgeValue(props.name) }).fill};
    }
  }

  .badge-name {
    background-color: rgba(0, 0, 0, .5);
    padding: 4px 8px;
    border-radius: 8px;
  }
`;
Badge.displayName = 'Badge';

const badgesort = (a, b) => getBadgeValue(b) - getBadgeValue(a);

const Badges = ({ userId }) => {
  const { t } = useTranslation(locales);

  const badges = useSelector(state => userSelectors.getBadges(state, userId), shallowEqual);

  return (
    <Wrapper>
      {badges.sort(badgesort).map(badge => (
        <Badge key={`badge-${badge}`} name={badge}>
          <Decagram />
          <div className="badge-name">{t(`badge.${badge}`)}</div>
        </Badge>
      ))}
    </Wrapper>
  );
};

Badges.propTypes = {
  userId: PropTypes.number.isRequired,
};

Badges.defaultProps = {
};

export default Badges;
