import React from 'react';
import PropTypes from 'prop-types';

import { useTranslation } from 'hooks';

import AlertContainer from '../AlertContainer';
import locales from '../i18n';

const ChannelDeleted = ({ channel, read }) => {
  const { t } = useTranslation(locales);

  return (
    <AlertContainer image={channel.avatar} read={read}>
      {channel.name}
      {' '}
      {t('has been deleted by owner')}
    </AlertContainer>
  );
};

ChannelDeleted.propTypes = {
  read: PropTypes.bool.isRequired,
  channel: PropTypes.shape({
    name: PropTypes.string.isRequired,
    avatar: PropTypes.string.isRequired,
  }).isRequired,
};

export default ChannelDeleted;
