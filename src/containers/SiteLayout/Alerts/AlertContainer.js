import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

import colors from 'utils/css/colors';

const AlertContainerWrapper = styled.div`
  border-left: 5px solid ${props => (props.read ? 'white' : colors.red)};
  min-height: 48px;
  border-radius: 0;
  padding: 2px 8px;
  border-bottom: 1px solid #eee;
  color: #333;
  width: 100%;
  box-sizing: border-box;
`;

const AlertLink = styled(Link)`
  display: flex;
  align-items: center;
  width: 100%;

.avatar {
  border-radius: 0;
  width: 40px;
  height: 40px;
  flex-shrink: 0;
}
`;

const AlertContainerImage = styled.img`
  width: 40px;
  height: 40px;
  flex-shrink: 0;
`;

const AlertContainerBody = styled.div`
  font-size: 14px;
  line-height: 16px;
  margin-left: 8px;
  margin-right: 5px;
  overflow: hidden;
  max-height: 32px;
  text-overflow: ellipsis;
  font-weight: 400;
  white-space: break-spaces;
`;

const AlertContainer = ({
  to,
  image,
  children,
  read,
}) => (
  <AlertContainerWrapper read={read}>
    <AlertLink to={to}>
      {image && (
        <>
          {typeof image === 'string'
            ? <AlertContainerImage src={image} />
            : image
          }
        </>
      )}
      <AlertContainerBody>
        {children}
      </AlertContainerBody>
    </AlertLink>
  </AlertContainerWrapper>
);

AlertContainer.propTypes = {
  to: PropTypes.string,
  image: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  children: PropTypes.node.isRequired,
  read: PropTypes.bool.isRequired,
};

AlertContainer.defaultProps = {
  image: null,
  to: null,
};

export default AlertContainer;
