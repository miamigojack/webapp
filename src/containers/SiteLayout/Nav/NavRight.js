import React from 'react';
import styled from 'styled-components';

import Search from './Search';
import Alerts from '../Alerts';
import UserMenu from './UserMenu';

const Wrapper = styled.ul`
  display: flex;
  align-items: center;
  height: 100%;
  margin: 0;
`;

const NavRight = () => (
  <Wrapper>
    <Search />
    <Alerts />
    <UserMenu />
  </Wrapper>
);

NavRight.propTypes = {
};

NavRight.defaultProps = {
};

export default NavRight;
