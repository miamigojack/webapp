import React, { useState, useCallback } from 'react';
import { useSelector } from 'react-redux';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

import { useTranslation } from 'hooks';
// import * as appSelectors from 'state/app/selectors';
// import * as appActions from 'state/app/actions';
import * as authSelectors from 'state/auth/selectors';
import * as invoiceSelectors from 'state/invoices/selectors';

import Avatar from 'components/UserAvatar';
import Menu, { Item } from 'components/Menu';
// import Toggle from 'components/Toggle';
import LanguageSelector from 'components/LanguageSelector';
import Badge from 'components/Badge';

import Invoices from './Invoices';
import NavItem from '../Item';
import locales from '../../i18n';

const NavAvatar = styled.div`
  width: 40px;
  margin: 0 0 0 10px;
  cursor: pointer;
  position: relative;

  img {
    /* border: 3px solid #c13e31; */
  }

  @media(max-width: 767px) {
    width: 32px;

    .avatar {
      width: 32px;
      height: 32px;

      img {
        width: 32px;
        height: 32px;
      }
    }
  }
`;

const UserMenu = React.memo(() => {
  const { t } = useTranslation(locales);
  // const dispatch = useDispatch();

  const loggedIn = useSelector(authSelectors.loggedIn);
  const meId = useSelector(authSelectors.selectId);
  const username = useSelector(authSelectors.selectUsername);
  const unpaidInvoicesCount = useSelector(invoiceSelectors.unpaidInvoicesCount);

  // const isSafeForWork = useSelector(appSelectors.selectIsSafeForWork);
  const [profileMenuOpened, setProfileMenuOpened] = useState(false);

  const openProfileMenu = useCallback(() => {
    setProfileMenuOpened(true);
  }, []);
  const closeProfileMenu = useCallback(() => {
    setProfileMenuOpened(false);
  }, []);
  // const toggleSafeForWork = useCallback(() => {
  //   dispatch(appActions.toggleSafeForWork());
  // }, [dispatch]);

  if (!loggedIn) return null;

  return (
    <NavItem onClick={openProfileMenu} pressed={profileMenuOpened}>
      <NavAvatar>
        <Avatar userId={meId} size="40px" showOnline={false} />
        <Menu
          maxHeight="auto"
          open={profileMenuOpened}
          onClose={closeProfileMenu}
          className="nav-dropdown"
        >
          <Item><Link to={`/@${username}`}>{t('See my profile')}</Link></Item>
          <Item><Link to="/user/edit/profile">{t('Edit my profile')}</Link></Item>
          <Invoices />
          <Item><Link to="/user/collections">{t('Collections')}</Link></Item>
          <Item><Link to="/user/relationships">{t('Relationships')}</Link></Item>
          <Item><Link to="/user/lists">{t('Contact lists')}</Link></Item>
          <Item><Link to="/user/checklist">{t('Checklist')}</Link></Item>
          {/* <Item><Link to="/bank">{t('SADEs')}</Link></Item> */}
          <Item><Link to="/user/notifications">{t('Notifications')}</Link></Item>
          <Item><Link to="/user/bots">{t('My chat bots')}</Link></Item>
          {/*
          <Item>
          <Toggle label={t('Safe for work')} active={isSafeForWork} onChange={toggleSafeForWork} />
          </Item>
          */}
          <Item><LanguageSelector /></Item>
          <Item><Link to="/logout">{t('Logout')}</Link></Item>
        </Menu>
        {unpaidInvoicesCount > 0 && <Badge value={unpaidInvoicesCount} />}
      </NavAvatar>
    </NavItem>
  );
});

export default UserMenu;
