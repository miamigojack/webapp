import React, { useCallback, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';

import { useTranslation, useInputValue } from 'hooks';

import Modal from 'components/Modal';
import Button from 'components/Button';
import Input from 'components/Forms/Input';

import locales from '../i18n';

const PasswordCheck = ({ onCancel, onConfirm }) => {
  const { t } = useTranslation(locales);

  const inputEl = useRef(null);

  const currentPassword = useInputValue('');

  useEffect(() => {
    if (inputEl.current) {
      inputEl.current.focus();
    }
  }, []);

  const confirm = useCallback(() => {
    onConfirm(currentPassword.value);
    onCancel();
  }, [onConfirm, onCancel, currentPassword.value]);

  return (
    <Modal
      title={t('Security check')}
      actions={[
        <Button key="security-check-confirm" onClick={confirm}>{t('global:Confirm')}</Button>,
      ]}
      onCancel={onCancel}
    >
      {t('For security reasons, please enter your current password in the input box below')}

      <Input placeholder={t('Current Password')} {...currentPassword} type="password" ref={inputEl} />
    </Modal>
  );
};

PasswordCheck.propTypes = {
  onCancel: PropTypes.func.isRequired,
  onConfirm: PropTypes.func.isRequired,
};

PasswordCheck.defaultProps = {
};

export default PasswordCheck;
