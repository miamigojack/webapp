import React, { useState, useCallback } from 'react';
import styled from 'styled-components';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import { useHistory } from 'react-router-dom';

import { useTranslation, useOpenClose } from 'hooks';
import * as authSelectors from 'state/auth/selectors';
import * as authActions from 'state/auth/actions';
import * as appActions from 'state/app/actions';

import UserTag from 'components/UserTag';
import Modal from 'components/Modal';
import Button from 'components/Button';

import { USERTAGS } from '../../../../constants';
import locales from '../i18n';

const Explanation = styled.div`
  font-size: 16px;
  margin-bottom: 32px;
`;
Explanation.displayName = 'Explanation';

const Wrapper = styled.div`
  margin: 32px !important;

  .usertags {
    flex-wrap: wrap;
    font-size: 14px;
    display: flex;

    > div {
      margin-bottom: 8px;
    }
  }
`;
Wrapper.displayName = 'TagsWrapper';

const Counter = styled.div`
  font-size: 14px;
  margin-top: 16px;
  margin-bottom: 16px;
  color: ${props => props.theme.colors.secondary};
`;
Counter.displayName = 'Counter';

const Tags = () => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();
  const history = useHistory();

  const userTags = useSelector(authSelectors.selectTags, shallowEqual);
  const username = useSelector(authSelectors.selectUsername);

  const [tags, setTags] = useState(userTags);
  const [showingMaxLimit, openShowingMaxLimit, closeShowingMaxLimit] = useOpenClose(false);
  const [saving, setSaving] = useState(false);

  const toggleSelect = useCallback(tag => () => {
    setTags((currentTags) => {
      if (currentTags.includes(tag)) return currentTags.filter(tt => tt !== tag);
      if (currentTags.length >= 3) {
        openShowingMaxLimit();
        return currentTags;
      }
      return [
        ...currentTags,
        tag,
      ];
    });
  }, [openShowingMaxLimit]);

  const save = useCallback(async () => {
    try {
      setSaving(true);
      await dispatch(authActions.updateTags(tags));
      history.replace(`/@${username}`);
      dispatch(appActions.addToast(t('Tags updated')));
    } catch (e) {
      setSaving(false);
      dispatch(appActions.addError(e));
    }
  }, [dispatch, history, t, tags, username]);

  const saveDisabled = tags.length === userTags.length && tags.every(tt => userTags.includes(tt));

  return (
    <>
      <Wrapper>
        <Explanation>
          {t('tags.explanation')}
        </Explanation>

        <div className="usertags">
          {Object.values(USERTAGS).map(tag => (
            <UserTag key={`usertag-general-${tag}`} dark selected={tags.includes(tag)} onClick={toggleSelect(tag)} role="button">
              {t(`global:TAG.${tag}`)}
            </UserTag>
          ))}
        </div>

        <Counter>
          {t('Selected tags {{selected}}/{{available}}', { selected: tags.length, available: 3 })}
        </Counter>

        <Button onClick={save} disabled={saveDisabled} loading={saving}>{t('global:Save')}</Button>
      </Wrapper>

      {/* Modals */}
      {showingMaxLimit && (
        <Modal onClose={closeShowingMaxLimit}>
          {t('You can only select a maximum of 3 user tags for your profile')}
        </Modal>
      )}
    </>
  );
};

Tags.propTypes = {
};

Tags.defaultProps = {
};

export default Tags;
