import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

import { useTitle } from 'hooks';
import * as authSelectors from 'state/auth/selectors';

import PageTitle from 'components/PageTitle';

const ListName = ({ listId }) => {
  const name = useSelector(state => authSelectors.selectUserListName(state, listId));
  useTitle(name);

  return <PageTitle>{name}</PageTitle>;
};

ListName.propTypes = {
  listId: PropTypes.string.isRequired,
};

ListName.defaultProps = {
};

export default ListName;
