import React from 'react';
import { useSelector, shallowEqual } from 'react-redux';

import { useTranslation, useTitle } from 'hooks';
import * as authSelectors from 'state/auth/selectors';

import Sidebar from 'containers/Sidebar';
import Layout from 'components/Layout';
import { FlexWrapper, FlexInnerWrapper, FlexContainer } from 'components/FlexWrapper';
import PageTitle from 'components/PageTitle';
import { SelectableList, SelectableListItem } from 'components/SelectableList';

import CreateButton from './CreateButton';
import locales from './i18n';

const Lists = () => {
  const { t } = useTranslation(locales);
  useTitle(t('User\'s Lists'));

  const lists = useSelector(authSelectors.selectUserLists, shallowEqual);

  return (
    <Layout columns={2} feed leftColumnOpen={false} rightColumnOpen={false}>
      <FlexWrapper canOverflow>
        <FlexInnerWrapper>
          <PageTitle>{t('User\'s Lists')}</PageTitle>
          <CreateButton />

          <FlexContainer framed>
            <SelectableList>
              {lists.map(list => (
                <SelectableListItem
                  title={list.name}
                  key={`follow-list-${list.id}`}
                  to={`/user/lists/${list.id}`}
                >
                  {t('{{count}} users', { count: list.users.length })}
                </SelectableListItem>
              ))}
            </SelectableList>
          </FlexContainer>
        </FlexInnerWrapper>
      </FlexWrapper>

      <Sidebar />
    </Layout>
  );
};

Lists.propTypes = {
};

Lists.defaultProps = {
};

export default Lists;
