const getBadgeColor = ({ theme: { badges }, badgesValue }) => {
  let slot = { value: 0, fill: '#AAAAAA', color: 'black' };
  badges.forEach((badge) => {
    if (badge.value <= badgesValue && badge.value >= slot.value) slot = badge;
  });

  return slot;
};

export default getBadgeColor;
